#ifndef __MAIH_H
#define __MAIH_H

#define BTN1_PORT	GPIOB
#define BTN1_PIN	GPIO_Pin_1
#define BTN2_PORT	GPIOE
#define BTN2_PIN	GPIO_Pin_3

#define LSE_STABILISATION_TIME	1000	// ms

void CLK_init(void);
void GPIO_init(void);
void RTC_init(void);
void I2C_init(void);

#endif