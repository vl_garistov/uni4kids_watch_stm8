# uni4kids_watch_stm8

A digital watch based on STM8L052C6.

### Notes for setting up the bloody IDE:
- Install [sublime-text](https://www.sublimetext.com/)
- Install [Package Control](https://packagecontrol.io/)
- Install [Deviot](https://packagecontrol.io/packages/Deviot%20(Arduino%20IDE)) plugin for Sublime
- Install [SublimeGDB](https://packagecontrol.io/packages/SublimeGDB) plugin for Sublime
- Install [PlatformIO Core (CLI)](https://docs.platformio.org/en/latest//core/index.html#piocore)
- From `platformio home` install ST STM8 platform
- Note that platformio installs its own copies of tools such as sdcc, gdb, stm8flash, etc. independantly of the system package manager
- Patch [stm8flash-git](https://aur.archlinux.org/packages/stm8flash-git/) according to issue [#139](https://github.com/vdudouyt/stm8flash/issues/139) before building it from source
- `cp /usr/bin/stm8flash ~/.platformio/packages/tool-stm8flash/`
- Add `--with-python=no` to the configure command in the PKGBUILD of [stm8-binutils-gdb](https://aur.archlinux.org/packages/stm8-binutils-gdb/) and build it from source
- `cp /usr/bin/stm8-gdb ~/.platformio/packages/tool-stm8binutils/bin/`
- Open TCP port 3333 on localhost

- Create custom board ~/.platformio/boards/uni4kids_watch_stm8_board.json :

	~~~
	{
		"build": {
			"core": "sduino",
			"extra_flags": "",
			"f_cpu": "1000000L",
			"cpu": "stm8",
			"mcu": "stm8l052c6"
		},
		"debug": {
			"svd_path": "STM8L052C6.svd",
			"openocd_target": "stm8l052",
			"default_tools": [
				"stlink"
			],
			"onboard_tools": [
				"stlink"
			]
		},
		"frameworks": [
			"spl"
		],
		"upload": {
			"maximum_ram_size": 2048,
			"maximum_size": 32768,
			"protocol": "stlinkv2",
			"protocols": [
				"stlinkv2",
				"serial"
			],
			"stm8flash_target": "stm8l052c6"
		},
		"name": "Uni4kids watch STM8",
		"url": "",
		"vendor": "Uni4kids"
	}
	~~~

- Create custom target ~/.platformio/packages/tool-openocd/scripts/target :

	~~~
	#config script for STM8L052

	set FLASHEND 0xFFFF
	set BLOCKSIZE 0x80
	set EEPROMSTART 0x1000
	set EEPROMEND 0x10ff

	proc stm8_reset_rop {} {
		mwb 0x4800 0xaa
		mwb 0x4800 0xaa
		reset halt
	}

	source [find target/stm8l.cfg]
	~~~

- [optional] `cp ~/.platformio/packages/tool-openocd/scripts/target /usr/share/openocd/scripts/target/stm8l052.cfg`

- Change /usr/share/openocd/scripts/interface/stlink.cfg to:

	~~~
	#
	# STMicroelectronics ST-LINK/V1, ST-LINK/V2, ST-LINK/V2-1, STLINK-V3 in-circuit
	# debugger/programmer
	#

	adapter driver st-link
	st-link vid_pid 0x0483 0x3744 0x0483 0x3748 0x0483 0x374b 0x0483 0x374d 0x0483 0x374e 0x0483 0x374f 0x0483 0x3752 0x0483 0x3753
	#adapter driver hla
	#hla_layout stlink
	#hla_device_desc "ST-LINK"
	#hla_vid_pid 0x0483 0x3744 0x0483 0x3748 0x0483 0x374b 0x0483 0x374d 0x0483 0x374e 0x0483 0x374f 0x0483 0x3752 0x0483 0x3753

	# Optionally specify the serial number of ST-LINK/V2 usb device.  ST-LINK/V2
	# devices seem to have serial numbers with unreadable characters.  ST-LINK/V2
	# firmware version >= V2.J21.S4 recommended to avoid issues with adapter serial
	# number reset issues.
	# eg.
	#hla_serial "\xaa\xbc\x6e\x06\x50\x75\xff\x55\x17\x42\x19\x3f"
	~~~
