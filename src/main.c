#include "main.h"
#include "stm8l15x_conf.h"
#include "stm8l15x_it.h"
#include "timing_delay.h"
#include "oled_hw_setup.h"
#include "oled_ssd1306.h"
#include "gfx_bw.h"
#include "fonts.h"
#include "uni4kids_watch.h"

void main(void)
{
	CLK_init();
	GPIO_init();
	TimingDelay_Init();
	enableInterrupts();
	Delay(LSE_STABILISATION_TIME);
	RTC_init();

	#ifdef SSD1306_I2C_CONTROL
	I2C_Config();
	#elif defined SSD1306_SPI_CONTROL
	SPI_Config();
	#endif
	SSD1306_Init();

	GFX_SetFont(font_6x4);
	GFX_SetFontSize(1);

	while(1)
	{
		test();
	}
}

void CLK_init(void)
{
	CLK_DeInit();

	// Select HSI as system clock source. This automatically turns it on.
	CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
	// Wait for HSI to start
	while (CLK_GetFlagStatus(CLK_FLAG_HSIRDY) != SET);
	// Perform the system clock switch
	CLK_SYSCLKSourceSwitchCmd(ENABLE);
	// Set the system clock prescaler to 16, resulting in 1 MHz system clock
	CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_16);

	// Turn on the LSE
	CLK_LSEConfig(CLK_LSE_ON);
	// Wait for LSE to start
	while (CLK_GetFlagStatus(CLK_FLAG_LSERDY) != SET);
	// Enable the RTC peripheral clock (gated instance of the system clock)
	CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
	// Set LSE with no prescaling as the RTC clock source
	CLK_RTCClockConfig(CLK_RTCCLKSource_LSE, CLK_RTCCLKDiv_1);

	// TODO: CSS

	// Disable unused clock sources and the clock output
	CLK_LSICmd(DISABLE);
	CLK_HSEConfig(CLK_HSE_OFF);
	CLK_CCOConfig(CLK_CCOSource_Off, CLK_CCODiv_1);

	// Enable peripheral clock for the OLED
	#ifdef SSD1306_SPI_CONTROL
	CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, ENABLE);
	#else
	CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, DISABLE);
	#endif
	#ifdef SSD1306_I2C_CONTROL
	CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, ENABLE);
	#else
	CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, DISABLE);
	#endif

	// Disable BOOT ROM peripheral clock
	CLK_PeripheralClockConfig(CLK_Peripheral_BOOTROM, DISABLE);
}

void GPIO_init(void)
{
	EXTI_DeInit();
	EXTI_SetPinSensitivity(EXTI_Pin_1, EXTI_Trigger_Falling);
	EXTI_SetPinSensitivity(EXTI_Pin_3, EXTI_Trigger_Falling);
	EXTI_ClearITPendingBit(EXTI_IT_Pin1);
	EXTI_ClearITPendingBit(EXTI_IT_Pin3);
	GPIO_Init(BTN1_PORT, BTN1_PIN, GPIO_Mode_In_FL_IT);
	GPIO_Init(BTN2_PORT, BTN2_PIN, GPIO_Mode_In_FL_IT);
	OLED_GPIO_Config();
}

void RTC_init(void)
{
	RTC_InitTypeDef RTC_init_params;
	RTC_TimeTypeDef RTC_time;
	RTC_DateTypeDef RTC_date;

	// Assumption: Clock control has already been configured and RTCCLK is 32.768 kHz
	RTC_init_params.RTC_HourFormat = RTC_HourFormat_24;
	RTC_init_params.RTC_AsynchPrediv = 0x7F;
	RTC_init_params.RTC_SynchPrediv = 0x00FF;
	RTC_Init(&RTC_init_params);

	RTC_DateStructInit(&RTC_date);
	RTC_date.RTC_WeekDay = RTC_Weekday_Saturday;
	RTC_date.RTC_Date = 07;
	RTC_date.RTC_Month = RTC_Month_August;
	RTC_date.RTC_Year = 21;
	RTC_SetDate(RTC_Format_BIN, &RTC_date);

	RTC_TimeStructInit(&RTC_time);
	RTC_time.RTC_Hours   = 12;
	RTC_time.RTC_Minutes = 00;
	RTC_time.RTC_Seconds = 00;
	RTC_SetTime(RTC_Format_BIN, &RTC_time);
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	   ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	(void) file;
	(void) line;
	/* Infinite loop */
	while (1);
}

#endif
